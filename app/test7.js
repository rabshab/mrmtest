export default function test7(string){
  //utilising the String.prototype property and creating a new function
  String.prototype.reverse=function(){
    //splitting to array and using array's reverse() function
    return this.split('').reverse().join('');
  };
  return string.reverse();
}

