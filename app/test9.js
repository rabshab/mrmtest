export default function countNodes(root) {

    //check if root node is null
    if (!root){
        return 0;
    }
    //check if no children
    else if ( !root.left && !root.right){
        return 1;
    }
    //recursively check children nodes until leaves are reached and add the root
    else{
        return 1 + countNodes(root.left) + countNodes(root.right);
    }


}
