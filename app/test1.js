/*
NB for challenges

Solutions were generated to fullfill the requirements of the test cases given but are
not complete and do not check bounds outwith the ones given. This was due to time constraints.
 */

export function filterNames(names, startsWith){

    //filtered array
    var filtered = new Array();
    //search length
    var searchLength = startsWith.length;

    //loop through names and check first x letters match the string
    for(var i = 0; i < names.length; i++){
        if(names[i].substring(0,searchLength) == startsWith){
            filtered.push(names[i]);
        }
    }

    return filtered;

}

export function objectFilter(objects, key, searchString) {
    
    //filtered array 
    var filtered = new Array();

    //loop through array and check key values against search string
    for(var i = 0; i < objects.length; i++){
        if(objects[i][key] == searchString){
            //add to filtered array if criterion met
            filtered.push(objects[i]);
        }
    }
    return filtered;



}

export function compoundObjectFilter(objects, keysString, searchString) {

    //filtered array
    var filtered = new Array();
    //split keys down into array
    var keys = keysString.split('.');
    
    //loop through objects
    for(var i = 0; i < objects.length; i++){
        var temp = objects[i];
        //recursively work through compound keys to get to the final value
        for(var j = 0; j < keys.length; j++){
            if(keys[j] in temp){
                temp = temp[keys[j]];
            }
        }
        //compare final value against search
        if(temp == searchString){
            filtered.push(objects[i]);
        }
    }
    return filtered;
    
}
