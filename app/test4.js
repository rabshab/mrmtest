//noinspection JSAnnotator
export default function getPrimes(max){

    //setup values
    //current set to 3 as cases lower than this are accounted for
    var primes = [], flag = true, current = 3;

    //checking for null and boundary values
    if(!max || max <=2){
        return primes;
    }
    //add lowest prime to array now that max has been checked
    primes.push(2);

    //loop through numbers between current and max and check if prime
    while(current<max){
        for(var i = 2; i<current; i++){
            //check if prime by looking at modulus. If zero for any for any value except 1 and itslef then not prime
            if (current%i == 0){
                flag = false;
            }
        }
        //check flag
        if(flag){
            primes.push(current);
        }
        //iterate and reset
        current++;
        flag = true;
    }

    return primes;



}
