export default function test8(){

    /*
    first thought
    We  can create an array of all the numbers between 1 and 100
    Adding the numbers at opposite ends of the array and moving in one increment gives the same value of 101
    We know there are 50 pairs therefore

     return (101 * 50);
    */

    /*
    second thought
    uses the same principle but extended to be more flexible
    slight hack to work around the one addition needed

     var maxCount = 100;
     var minCount = 1
     return((maxCount-(-minCount)) * (maxCount / 2));
     */

    /*
    Third thought
    I guess if the -(- trick is ok then we might as well just do it the natural way with a loop
    as the previous version is not really adding all the numbers.
     */
    var maxCount = 100;
    var minCount = 1;
    var count = 0;

    for(var i = maxCount; i>=minCount; i--){
        count = count-(-i);
    }

    return count;





}
