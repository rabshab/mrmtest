export function capitalise(names){

    //loop through names and convert to upper case using String toUpperCase() method
    for(var i = 0; i < names.length; i++){
        names[i] = names[i].toUpperCase();
    }
    return names;

}

export function extractValue(objects, key) {

    //new array of values
    var keyValues = new Array();
    //loop through object array and add values to new array
    for(var i = 0; i < objects.length; i++){
        keyValues.push(objects[i][key]) ;
    }
    return keyValues;

}

export function extractCompoundValue(objects, keysString) {
    
    //new array
    var filtered = new Array();
    //split compound keys as before
    var keys = keysString.split('.');

    //same approach as seen in test1.js
    for(var i = 0; i < objects.length; i++){
        var temp = objects[i];
        for(var j = 0; j < keys.length; j++){
            if(keys[j] in temp){
                temp = temp[keys[j]];
            }
        }
        filtered.push(temp);
    }

    return filtered;
    
}
