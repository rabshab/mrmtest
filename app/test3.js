//noinspection JSAnnotator
export default function test3(numMax){
    
    //array to contain the fibonacci sequence
    var fibArray = new Array();

    //checking for null or '0' inputs
    if(!numMax || numMax == 0) {
        return fibArray;
    }
    
    //starter values to begin the sequence
    var element1 = 0;
    var element2 = 1;
    var loops = 2;

    //checking for input of '1' 
    fibArray.push(element1);
    if(numMax == 1){
        return fibArray;
    }
    fibArray.push(element2);

    //now that the sequence has two confirmed elements a recursive solution can be generated
    while(loops<numMax){
        var temp = element1 + element2;
        fibArray.push(temp);
        element1 = element2;
        element2 = temp;
        loops++;
    }
    
    return fibArray;


}
