export function groupBy(people, key) {

    // No specification is given for this function in test5_spec.js

}

export function groupBySex(people) {

    //new object to return
    var newOb = {};

    //loop through people and examine the key values
    for(var i = 0; i<people.length ; i++){
        var myKey = people[i].sex;
        //check if the new object has the key. If not then add and initialise
        if(!(myKey in newOb)){
            newOb[myKey] = new Array();
        }
        //add value to new object under correct key
        newOb[myKey].push(people[i]);
    }

    return newOb;
}

export function groupByYearThenSex(people) {

    //new object to return
    var newOb = {};

    //loop through people
    for(var i = 0; i<people.length ; i++){
        //first key for the new object
        var myFirstKey = people[i].born;
        //check if new object has the key. If not then add and initialise
        if(!(myFirstKey in newOb)){
            newOb[myFirstKey] = {};
        }
        //second key for the new object
        var mySecondKey = people[i].sex;
        //check if new object has the key. If not then add and initialise
        if(!(mySecondKey in newOb[myFirstKey])){
            newOb[myFirstKey][mySecondKey] = [];
        }
        //add value to new object under correct keys
        newOb[myFirstKey][mySecondKey].push(people[i]);
    }

    return newOb;

}
